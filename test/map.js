const MAPToken = artifacts.require('MAP.sol');
const Crowdsale = artifacts.require('Crowdsale.sol');
var Web3 = require("web3");
var web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));

//account 0 owner
//account 1 team
//account 2 advisor
//account 3 company
//account 4 Wallet
//account 5 beneficeary presale
//account 6 beneficeary Crowdsale one
//account 7 beneficeary Crowdsale two
//account 8 beneficeary Crowdsale three
//account 9 beneficeary of BountyTokens
//hardcap softcap left to be check
//to run test cases vesting period and token lock period =1

contract('Maester Protocol', async (accounts) => {

  it('Should correctly initialize constructor values of Maester protocol Token Contract', async () => {

    this.tokenhold = await MAPToken.new(accounts[0], accounts[1], accounts[2], accounts[3]);
    let totalSupply = await this.tokenhold.totalSupply_.call();
    let owner = await this.tokenhold.owner.call();
    let teamAddress = await this.tokenhold.team.call();
    let advisorsAddress = await this.tokenhold.advisors.call();
    let companyAddress = await this.tokenhold.companyAddress.call();
    assert.equal(totalSupply.toNumber(), 500000000000000000000000000);
    assert.equal(owner, accounts[0]);
    assert.equal(teamAddress, accounts[1]);
    assert.equal(advisorsAddress, accounts[2]);
    assert.equal(companyAddress, accounts[3]);
  });

  it("Should Deploy Crowdsale only", async () => {

    this.crowdhold = await Crowdsale.new(accounts[0], accounts[4], this.tokenhold.address, 20000, { gas: 600000000 });

  });

  it("Should Activate Sale contract", async () => {

    var Activate = await this.tokenhold.activateSaleContract(this.crowdhold.address, { gas: 500000000 });

  });

  it("Should Listing Vesting Tokens for team", async () => {

    var listingStatus = await this.tokenhold.listingDate.call();
    assert.equal(listingStatus.toNumber(), 0);
    var Listing = await this.tokenhold.isListing({ gas: 500000000 });
    var listingStatus1 = await this.tokenhold.listingDate.call();
  });

  it("Should check balance of Crowdsale after, crowdsale activate from token contract", async () => {

    let balancOfCrowdsale = 300000000000000000000000000;
    var balance = await this.tokenhold.balanceOf.call(this.crowdhold.address, { gas: 500000000 });
    assert.equal(balance.toNumber(), balancOfCrowdsale);

  });

  it("Should Start PreSale ", async () => {

    var PreSaleStart = await this.crowdhold.startPresale({ from: accounts[0] });
    var getTheStage = await this.crowdhold.getStage.call();
    var _presale = 'Presale start';
    assert.equal(getTheStage, _presale, 'Presale start');

  });

  it("Should be able to buy Tokens  according to preSale", async () => {

    let fundWalletBefore = await web3.eth.getBalance(accounts[4]);
    let AccountBalance_oneBefore = await web3.eth.getBalance(accounts[5]);
    var buy_Tokens = await this.crowdhold.buyTokens(accounts[5], { from: accounts[5], value: web3.toWei("1", "ether") });
    var tokens = 2600;
    var balance_after = await this.tokenhold.balanceOf.call(accounts[5]);
    let fundWalletAfter = await web3.eth.getBalance(accounts[4]);
    let AccountBalance_oneAfter = await web3.eth.getBalance(accounts[5]);
    assert.equal(balance_after.toNumber(), tokens * (10 ** 18), 'Token ammount');

  });

  it("Should be able to end pre sale ,start Crowd sale round One and buy tokens according to that", async () => {
    await this.crowdhold.endPreSale();
    let stage = await this.crowdhold.getStage();
    assert.equal(stage, "Presale end", "Stage is wrong");
    await this.crowdhold.startCrowdSaleStageOne();
    let newStage = await this.crowdhold.getStage();
    assert.equal(newStage, "Crowdsale stage one start", "Stage is wrong");
    var balance_Before = await this.tokenhold.balanceOf.call(accounts[6]);
    var buy_Tokens = await this.crowdhold.buyTokens(accounts[6], { from: accounts[6], value: web3.toWei("1", "ether") });
    var tokens = 2300;
    var balance_after = await this.tokenhold.balanceOf.call(accounts[6]);
    let fundWalletAfter = await web3.eth.getBalance(accounts[4]);
    let AccountBalance_oneAfter = await web3.eth.getBalance(accounts[6]);
    assert.equal(balance_after.toNumber(), balance_Before.toNumber() + (tokens * (10 ** 18)), 'Token ammount');
  });

  it("Should be able to end Crowdsale Round one ,start Crowd sale round Two and buy tokens according to that", async () => {
    await this.crowdhold.endCrowdSaleStageOne();
    let stage1 = await this.crowdhold.getStage();
    assert.equal(stage1, "Crowdsale stage one End", "Stage is wrong");
    await this.crowdhold.startCrowdSaleStageTwo();
    let newStage1 = await this.crowdhold.getStage();
    assert.equal(newStage1, "Crowdsale stage two start", "Stage is wrong");
    var balance_Before1 = await this.tokenhold.balanceOf.call(accounts[7]);
    var buy_Tokens1 = await this.crowdhold.buyTokens(accounts[7], { from: accounts[7], value: web3.toWei("1", "ether") });
    var tokens1 = 2200;
    var balance_after1 = await this.tokenhold.balanceOf.call(accounts[7]);
    let fundWalletAfter1 = await web3.eth.getBalance(accounts[4]);
    let AccountBalance_oneAfter1 = await web3.eth.getBalance(accounts[7]);
    assert.equal(balance_after1.toNumber(), balance_Before1.toNumber() + (tokens1 * (10 ** 18)), 'Token ammount');


  });

  it("Should be able to end Crowdsale Round two ,start Crowd sale round Three and buy tokens according to that", async () => {
    await this.crowdhold.endCrowdSaleStageTwo();
    let stage2 = await this.crowdhold.getStage();
    assert.equal(stage2, "Crowdsale stage two End", "Stage is wrong");
    await this.crowdhold.startCrowdSaleStageThree();
    let newStage2 = await this.crowdhold.getStage();
    assert.equal(newStage2, "Crowdsale stage three startd", "Stage is wrong");
    var balance_Before2 = await this.tokenhold.balanceOf.call(accounts[8]);
    var buy_Tokens2 = await this.crowdhold.buyTokens(accounts[8], { from: accounts[8], value: web3.toWei("1", "ether") });
    var tokens2 = 2000;
    var balance_after2 = await this.tokenhold.balanceOf.call(accounts[8]);
    let fundWalletAfter2 = await web3.eth.getBalance(accounts[4]);
    let AccountBalance_oneAfter2 = await web3.eth.getBalance(accounts[8]);
    assert.equal(balance_after2.toNumber(), balance_Before2.toNumber() + (tokens2 * (10 ** 18)), 'Token ammount');


  });

  it("Should be able to end Crowdsale Round three, and Check balace of crowdsale contract", async () => {

    var balance_Crowdsale = await this.tokenhold.balanceOf.call(this.crowdhold.address);
    assert.equal(balance_Crowdsale.toNumber(), 299990900000000000000000000, "balance is Crowdsale tokens");
    await this.crowdhold.endCrowdSaleStageThree();
    let stage3 = await this.crowdhold.getStage();
    var balance_Crowdsale = await this.tokenhold.balanceOf.call(this.crowdhold.address);
    assert.equal(balance_Crowdsale, 0, "balance is zero");
  });

  it("Should Pause and restart Crowdsale ", async () => {

    var getTheStage = await this.crowdhold.getStage.call();
    var PauseSale = await this.crowdhold.pause();
    var getTheStageNow = await this.crowdhold.getStage.call();
    var _pause = 'pause';
    assert.equal(getTheStageNow, _pause, 'pause');
    var RestartSale = await this.crowdhold.restartSale();
    var getTheStageNow1 = await this.crowdhold.getStage.call();
    var CrowdStatus = 'Crowdsale stage three End';
    assert.equal(getTheStageNow1, CrowdStatus, 'Crowdsale stage three End');

  });

  it("Should be able to set ether price ", async () => {

    var currentEthprice = 20000;
    var toBeEthprice = 50000;
    var ethpricebefore = await this.crowdhold.ethPrice.call();
    assert.equal(ethpricebefore.toNumber(), currentEthprice, 'ether price before');
    var setEthPrice = await this.crowdhold.setEthPriceInCents(50000, { from: accounts[0] });
    var ethpricenow = await this.crowdhold.ethPrice.call();
    assert.equal(ethpricenow.toNumber(), toBeEthprice, 'ether price After');

  });

  it("Should revert if owner tries to transfer zero bounty  ", async () => {

    var sendbountyTokens1 = await this.tokenhold.sendBounty(accounts[9], 0, { gas: 5000000 });
    var BountySendValue1 = 0;
    var balanceOfbounty1 = await this.tokenhold.balanceOf.call(accounts[9]);
    assert.equal(balanceOfbounty1.toNumber(), BountySendValue1, 'Bounty Send');

  });

  it("Should revert if user tries to send Negative Bounty Tokens  ", async () => {
    try {
      let initialBounty = await this.tokenhold.bountyTokens.call();
      var sendbountyTokens = await this.tokenhold.sendBounty(accounts[9], -5, { gas: 5000000 });
    } catch (error) {
      var error_ = 'VM Exception while processing transaction: invalid opcode';
      assert.equal(error.message, error_, 'Token ammount');
    }
  });

  it("Should send Bounty Tokens  ", async () => {
    let initialBounty = await this.tokenhold.bountyTokens.call();
    var sendbountyTokens = await this.tokenhold.sendBounty(accounts[9], 5, { gas: 5000000 });
    var BountySendValue = 5;
    var balanceOfbounty = await this.tokenhold.balanceOf.call(accounts[9]);
    let bountyLeft = await this.tokenhold.bountyTokens.call();
    assert.equal(balanceOfbounty.toNumber() / 10 ** 18, BountySendValue, 'Wrong bounty sent');
  });

  it("Should Burn account[9] Bounty Tokens  ", async () => {

    let totalSupplyBefore = await this.tokenhold.totalSupply_.call();
    var BurnTokens = await this.tokenhold.burn(2, { from: accounts[9], gas: 5000000 });
    var BountySendValue = 3;
    var balanceAfterBurn = await this.tokenhold.balanceOf.call(accounts[9]);
    let totalSupply1 = await this.tokenhold.totalSupply_.call();
    assert.equal(balanceAfterBurn.toNumber() / 10 ** 18, BountySendValue, 'Wrong bounty sent');

  });

  it("Should return if User tries to Burn Negative tokens ", async () => {

    try {
      let totalSupplyBefore1 = await this.tokenhold.totalSupply_.call();
      var balanceBeforeBurn1 = await this.tokenhold.balanceOf.call(accounts[9]);
      var BurnTokens1 = await this.tokenhold.burn(-1, { from: accounts[9], gas: 5000000 });
      var balanceAfterBurn1 = await this.tokenhold.balanceOf.call(accounts[9]);

    } catch (error) {
      var error_ = 'VM Exception while processing transaction: invalid opcode';
      assert.equal(error.message, error_, 'Token ammount');

    }
  });

  it("Should return if User tries to Burn zero tokens ", async () => {

    let totalSupplyBefore = await this.tokenhold.totalSupply_.call();
    var balanceBeforeBurn = await this.tokenhold.balanceOf.call(accounts[9]);
    var BurnTokens = await this.tokenhold.burn(0, { from: accounts[9], gas: 5000000 });
    var balanceAfterBurn = await this.tokenhold.balanceOf.call(accounts[9]);

  });



  it("Should Check if SoftCap Reached or not ", async () => {

    var Softcap = await this.crowdhold.softCap.call();
    var tokenSold = await this.crowdhold.totalTokenSold.call();
    var IsSoftCapReached = await this.crowdhold.isSoftcapReached.call();
    var _presale = false;
    assert.equal(IsSoftCapReached, _presale, 'Soft Cap Reached');

  });

  it("Should send advisors Tokens  ", async () => {

    let advisorsTokens = await this.tokenhold.advisorsTokens.call();
    var sendadvisorsTokens = await this.tokenhold.releaseAdvisorsTokens({ gas: 5000000 });
    var BountySendValue1 = 7000000;
    var balanceOfbounty1 = await this.tokenhold.balanceOf.call(accounts[2]);
    let bountyLeft1 = await this.tokenhold.bountyTokens.call();
    assert.equal(balanceOfbounty1.toNumber() / 10 ** 18, BountySendValue1, 'Wrong bounty sent');


  });

  it("Should Get Vesting Period Number", async () => {

    var listingStatus2 = await this.tokenhold.listingDate.call();
    var VestingPeriod = await this.tokenhold.getVestingPeriodNumber();

  });

  it("Should send Team Tokens  ", async () => {

    var listingStatus21 = await this.tokenhold.listingDate.call();
    var VestingPeriod1 = await this.tokenhold.getVestingPeriodNumber();
    let teamTokens = await this.tokenhold.teamTokens.call();
    var sendTeamTokens = await this.tokenhold.releaseTeamTokens({ gas: 5000000 });
    var TeamTokensValue1 = 100000000;
    var TeamTokens1 = await this.tokenhold.balanceOf.call(accounts[1]);
    let TeamTokensReleased = await this.tokenhold.teamReleased.call();
    assert.equal(TeamTokens1.toNumber() / 10 ** 18, TeamTokensValue1, 'Wrong bounty sent');

  });

  it("Should be able to transfer ownership of Token Contract ", async () => {

    let ownerOld = await this.tokenhold.owner.call();
    let newowner = await this.tokenhold.transferOwnership(accounts[9], { from: accounts[0] });
    let ownerNew = await this.tokenhold.owner.call();
    assert.equal(ownerNew, accounts[9], 'Transfered ownership');
  });

  it("Should be able to transfer Tokens from one beneficeary to another when token lock period is over ", async () => {

    var SenderTokens = await this.tokenhold.balanceOf.call(accounts[7]);
    //console.log(SenderTokens.toNumber(), 'Sender Tokens before');
    var RecieverTokens = await this.tokenhold.balanceOf.call(accounts[9]);
    //console.log(RecieverTokens.toNumber(), 'Reciever Tokens before');
    var transferTokens = await this.tokenhold.transfer(accounts[9], 1000000000000000000, { from: accounts[7] });
    var SenderTokensafter = await this.tokenhold.balanceOf.call(accounts[7]);
    //console.log(SenderTokensafter.toNumber(), 'Sender Tokens after');
    var RecieverTokensafter = await this.tokenhold.balanceOf.call(accounts[9]);
    //console.log(RecieverTokensafter.toNumber(), 'Reciever Tokens after');
  });

  it("should Approve address to spend specific token ", async () => {

    this.tokenhold.approve(accounts[9], 1000000000000000000, { from: accounts[7] });
    let allowance = await this.tokenhold.allowance.call(accounts[7], accounts[9]);
    assert.equal(allowance, 1000000000000000000, "allowance is wrong when approve");

  });

  it("should increase Approval ", async () => {

    let allowance1 = await this.tokenhold.allowance.call(accounts[7], accounts[9]);
    assert.equal(allowance1, 1000000000000000000, "allowance is wrong when increase approval");
    this.tokenhold.increaseApproval(accounts[9], 1000000000000000000, { from: accounts[7] });
    let allowanceNew = await this.tokenhold.allowance.call(accounts[7], accounts[9]);
    assert.equal(allowanceNew, 2000000000000000000, "allowance is wrong when increase approval done");

  });

  it("should decrease Approval ", async () => {

    let allowance12 = await this.tokenhold.allowance.call(accounts[7], accounts[9]);
    //console.log(allowance12.toNumber(), 'Reciever Tokens after');
    assert.equal(allowance12, 2000000000000000000, "allowance is wrong when increase approval");
    this.tokenhold.decreaseApproval(accounts[9], 1000000000000000000, { from: accounts[7] });
    let allowanceNew = await this.tokenhold.allowance.call(accounts[7], accounts[9]);
    assert.equal(allowanceNew, 1000000000000000000, "allowance is wrong when increase approval done");

  });

  it("should not increase Approval for Negative Tokens", async () => {

    try {

      this.tokenhold.increaseApproval(accounts[9], -1000000000000000000, { from: accounts[7] });

    }
    catch (error) {
      var error_ = 'VM Exception while processing transaction: invalid opcode';
      assert.equal(error.message, error_, 'Token ammount');

    }

  });

  it("should Not Approve address to spend Negative token ", async () => {

    try {
      this.tokenhold.approve(accounts[9], -1000000000000000000, { from: accounts[7] });
    } catch (error) {
      var error_ = 'VM Exception while processing transaction: invalid opcode';
      assert.equal(error.message, error_, 'Token ammount');
    }
  });

})
