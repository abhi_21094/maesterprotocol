pragma solidity ^0.4.24;
import "./OraclizeI.sol";
import "./Ownable.sol";
contract PriceTicker is usingOraclize, Ownable {

    uint256 public ethPrice; // 1 Ether price in USD cents.
    uint256 constant CUSTOM_GASLIMIT = 150000;
    uint256 public updateTime = 0;

    event LogConstructorInitiated(string nextStep);
    event newOraclizeQuery(string description);
    event newPriceTicker(bytes32 myid, string price, bytes proof);


    function PriceTicker() public {
       // oraclize_setProof(proofType_TLSNotary | proofStorage_IPFS);
       // LogConstructorInitiated("Constructor was initiated. Call 'update()' to send the Oraclize Query.");
    }

    function __callback(bytes32 myid, string result, bytes proof) public {
        if (msg.sender != oraclize_cbAddress()) revert();
        ethPrice = parseInt(result, 2);
        newPriceTicker(myid, result, proof);
        if (updateTime > 0) updateAfter(updateTime);
    }

    function update() public onlyOwner {
        if (updateTime > 0) updateTime = 0;
        if (oraclize_getPrice("URL", CUSTOM_GASLIMIT) > this.balance) {
            newOraclizeQuery("Oraclize query was NOT sent, please add some ETH to cover for the query fee");
        } else {
            newOraclizeQuery("Oraclize query was sent, standing by for the answer..");
            oraclize_query("URL", "json(https://api.kraken.com/0/public/Ticker?pair=ETHUSD).result.XETHZUSD.c.0", CUSTOM_GASLIMIT);
        }
    }

    function updatePeriodically(uint256 _updateTime) public onlyOwner {
        updateTime = _updateTime;
        if (oraclize_getPrice("URL", CUSTOM_GASLIMIT) > this.balance) {
            newOraclizeQuery("Oraclize query was NOT sent, please add some ETH to cover for the query fee");
        } else {
            newOraclizeQuery("Oraclize query was sent, standing by for the answer..");
            oraclize_query("URL", "json(https://api.kraken.com/0/public/Ticker?pair=ETHUSD).result.XETHZUSD.c.0", CUSTOM_GASLIMIT);
        }
    }

    function updateAfter(uint256 _updateTime) internal {
        if (oraclize_getPrice("URL", CUSTOM_GASLIMIT) > this.balance) {
            newOraclizeQuery("Oraclize query was NOT sent, please add some ETH to cover for the query fee");
        } else {
            newOraclizeQuery("Oraclize query was sent, standing by for the answer..");
            oraclize_query(_updateTime, "URL", "json(https://api.kraken.com/0/public/Ticker?pair=ETHUSD).result.XETHZUSD.c.0", CUSTOM_GASLIMIT);
        }
    }
}