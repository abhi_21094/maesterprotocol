pragma solidity ^0.4.24;
import "./Ownable.sol";
contract Token {
  function saleTransfer(address _to, uint256 _value) public returns (bool);
  function burnTokensForSale() public returns (bool);
}

/**
 * @title SafeMath
 * @dev Math operations with safety checks that throw on error
 */
library SafeMath {

  /**
  * @dev Multiplies two numbers, throws on overflow.
  */
  function mul(uint256 a, uint256 b) internal pure returns (uint256) {
    if (a == 0) {
      return 0;
    }
    uint256 c = a * b;
    assert(c / a == b);
    return c;
  }

  /**
  * @dev Integer division of two numbers, truncating the quotient.
  */
  function div(uint256 a, uint256 b) internal pure returns (uint256) {
    // assert(b > 0); // Solidity automatically throws when dividing by 0
    uint256 c = a / b;
    // assert(a == b * c + a % b); // There is no case in which this doesn't hold
    return c;
  }

  /**
  * @dev Substracts two numbers, throws on overflow (i.e. if subtrahend is greater than minuend).
  */
  function sub(uint256 a, uint256 b) internal pure returns (uint256) {
    assert(b <= a);
    return a - b;
  }

  /**
  * @dev Adds two numbers, throws on overflow.
  */
  function add(uint256 a, uint256 b) internal pure returns (uint256) {
    uint256 c = a + b;
    assert(c >= a);
    return c;
  }
}

/**
 * @title Crowdsale
 * @dev Crowdsale is a base contract for managing a token crowdsale,
 * allowing investors to purchase tokens with ether. This contract implements
 * such functionality in its most fundamental form and can be extended to provide additional
 * functionality and/or custom behavior.
 * The external interface represents the basic interface for purchasing tokens, and conform
 * the base architecture for crowdsales. They are *not* intended to be modified / overriden.
 * The internal interface conforms the extensible and modifiable surface of crowdsales. Override 
 * the methods to add functionality. Consider using 'super' where appropiate to concatenate
 * behavior.
 */

contract Crowdsale is Ownable  {
  using SafeMath for uint256;
  
  uint256 public ethPrice;

  // The token being sold
  Token public token;

  // Address where funds are collected
  address public wallet;

  // Amount of wei raised
  uint256 public weiRaised;
  // Amount of usd in cents Raised
  uint256 public RaisedInCents;
  
  enum Stages { preSaleNotStart ,pause,PreSaleStart, PreSaleEnd, crowdsaleStageOneStart, crowdsaleStageOneEnd, crowdsaleStageTwoStart, crowdsaleStageTwoEnd, crowdsaleStageThreeStart, crowdsaleStageThreeEnd}

  Stages previousStage;
  Stages currentStage;
  //Hardcap and softcap of a sale is define
  uint256 public hardCap=300000000;
  uint256 public softCap=10000000;
  
  //No of tokens sold in each phase
  uint256 presaleTokensold =0;
  uint256 crowdsaleTokenSold_One=0;
  uint256 crowdsaleTokenSold_Two=0;
  uint256 crowdsaleTokenSold_Three=0;
  
uint256 public  totalTokenSold=presaleTokensold.add(crowdsaleTokenSold_One).add(crowdsaleTokenSold_Two).add(crowdsaleTokenSold_Three);
  
  //percentage of bonus in different phases
  uint32 presaleBonuses = 30;
  uint32 stageOneBonuses = 15;
  uint32 stageTwoBonuses = 10;
  uint32 stageThreeBonuses = 0;
  
  uint256 tokenPrice = 10; // price in USD cents, 1 token = 0.1 USD

  /**
   * Event for token purchase logging
   * @param purchaser who paid for the tokens
   * @param beneficiary who got the tokens
   * @param value weis paid for purchase
   * @param amount amount of tokens purchased
   */
  event TokenPurchase(address indexed purchaser, address indexed beneficiary, uint256 value, uint256 amount);

  /**
   * @param _wallet Address where collected funds will be forwarded to
   * @param _wallet Address to collect funds
   * @param _token Address of the token being sold
   */
  function Crowdsale(address _newOwner, address _wallet, Token _token,uint256 _ethPriceInCents) public 
  {
    require(_wallet != address(0));
    require(_token != address(0));
    //update();
    ethPrice = _ethPriceInCents;
    wallet = _wallet;
    currentStage=Stages.preSaleNotStart;
    owner = _newOwner;
    token = _token;
    
  }

  function isSoftcapReached() public view returns(bool)
  {
      if (totalTokenSold>=softCap)
      {
          return true;
          
      }
      else
      {
          return false;
          
      }

  }
  function pause() public onlyOwner
  {
      previousStage=currentStage;
      currentStage=Stages.pause;
  }
  
  function restartSale() public onlyOwner
  {
     require(currentStage==Stages.pause);
     currentStage=previousStage;
  }
  
  function startPresale() public onlyOwner
  {
    require(currentStage==Stages.preSaleNotStart);
    currentStage = Stages.PreSaleStart;  
  }

  function endPreSale() public onlyOwner 
  {
    require(currentStage == Stages.PreSaleStart);
    currentStage = Stages.PreSaleEnd;
  }

  function startCrowdSaleStageOne() public onlyOwner 
  {
    require(currentStage == Stages.PreSaleEnd);
    currentStage = Stages.crowdsaleStageOneStart;
   
  }

  function endCrowdSaleStageOne() public onlyOwner
  {
    require(currentStage == Stages.crowdsaleStageOneStart);
    currentStage = Stages.crowdsaleStageOneEnd;
  }

  function startCrowdSaleStageTwo() public onlyOwner 
  {
    require(currentStage == Stages.crowdsaleStageOneEnd);
    currentStage = Stages.crowdsaleStageTwoStart;

  }

function endCrowdSaleStageTwo() public onlyOwner
  {
    require(currentStage == Stages.crowdsaleStageTwoStart);
    currentStage = Stages.crowdsaleStageTwoEnd;
  }

  function startCrowdSaleStageThree() public onlyOwner 
  {
    require(currentStage == Stages.crowdsaleStageTwoEnd);
    currentStage = Stages.crowdsaleStageThreeStart;

  }

  function endCrowdSaleStageThree() public onlyOwner {
    require(currentStage == Stages.crowdsaleStageThreeStart);
    currentStage = Stages.crowdsaleStageThreeEnd;
    require(token.burnTokensForSale());
  }

  /**
   * @dev Get stage of contract .
   */

  function getStage() public view returns (string) {
    if (currentStage == Stages.PreSaleStart) return 'Presale start';
    else if (currentStage == Stages.PreSaleEnd) return 'Presale end';
    else if (currentStage == Stages.crowdsaleStageOneStart) return 'Crowdsale stage one start';
    else if (currentStage == Stages.crowdsaleStageOneEnd) return 'Crowdsale stage one End';
    else if (currentStage == Stages.crowdsaleStageTwoStart) return 'Crowdsale stage two start';
    else if (currentStage == Stages.crowdsaleStageTwoEnd) return 'Crowdsale stage two End';
    else if (currentStage == Stages.crowdsaleStageThreeStart) return 'Crowdsale stage three startd';
    else if (currentStage == Stages.crowdsaleStageThreeEnd) return 'Crowdsale stage three End';
    else if (currentStage == Stages.pause) return 'pause';

  }

  // -----------------------------------------
  // Crowdsale external interface
  // -----------------------------------------

  /**
   * @dev fallback function ***DO NOT OVERRIDE***
   */
  function () external payable {
    if (msg.sender != owner) buyTokens(msg.sender);
  }

  /**
   * @param _beneficiary Address performing the token purchase
   */
  function buyTokens(address _beneficiary) public payable {
    uint256 weiAmount = msg.value;
    require(weiAmount > 0);
    require(currentStage!=Stages.pause);
    require(ethPrice > 0);
    uint256 usdCents = weiAmount.mul(ethPrice).div(1 ether); 
    

    // calculate token amount to be created
    uint256 tokens = _getTokenAmount(usdCents);
_preValidatePurchase(_beneficiary, tokens);
    _validateTokensLimits(tokens);

    // update state
    weiRaised = weiRaised.add(weiAmount);
    RaisedInCents=RaisedInCents.add(usdCents);
    
    _processPurchase(_beneficiary, tokens);
    emit TokenPurchase(msg.sender, _beneficiary, weiAmount, tokens);
    _forwardFunds();
  }

  // -----------------------------------------
  // Internal interface (extensible)
  // -----------------------------------------

  /**
   * @dev Validation of an incoming purchase. Use require statemens to revert state when conditions are not met. Use super to concatenate validations.
   * @param _beneficiary Address performing the token purchase
   * @param _tokens Value in tokens involved in the purchase
   */
  function _preValidatePurchase(address _beneficiary, uint256 _tokens) internal view 
  {
    require(_beneficiary != address(0));
    if (currentStage == Stages.PreSaleStart) 
    {
      require(_tokens >= 15000);//150000  
     
    }
    else if (currentStage == Stages.crowdsaleStageOneStart) 
    {
      require(_tokens >= 500); //5000 
     
    }
    else if (currentStage == Stages.crowdsaleStageTwoStart) 
    {
      require(_tokens >= 500); //5000 
     
    }
    
    else if (currentStage == Stages.crowdsaleStageThreeStart) 
    {
      require(_tokens >= 500);  //5000
     
    }
    else
    {
      revert();
    }
  }

  /**
   * @dev Validation of the capped restrictions.
   * @param _tokens tokens amount
   */
  function _validateTokensLimits(uint256 _tokens) internal 
  {
    if (currentStage == Stages.PreSaleStart) 
    {
      presaleTokensold = presaleTokensold.add(_tokens);
      require(totalTokenSold <= hardCap);
    } 
    else if(currentStage == Stages.crowdsaleStageOneStart) 
    {
      crowdsaleTokenSold_One = crowdsaleTokenSold_One.add(_tokens);
      require(totalTokenSold <= hardCap);
    }
    else if(currentStage == Stages.crowdsaleStageTwoStart) 
    {
      crowdsaleTokenSold_Two = crowdsaleTokenSold_Two.add(_tokens);
      require(totalTokenSold <= hardCap);
    }
    
    else if(currentStage == Stages.crowdsaleStageThreeStart) 
    {
      crowdsaleTokenSold_Three = crowdsaleTokenSold_Three.add(_tokens);
      require(totalTokenSold <= hardCap);
    }
    else
    {
      revert();
    }
  }

  /**
   * @dev Source of tokens. Override this method to modify the way in which the crowdsale ultimately gets and sends its tokens.
   * @param _beneficiary Address performing the token purchase
   * @param _tokenAmount Number of tokens to be emitted
   */
  function _deliverTokens(address _beneficiary, uint256 _tokenAmount) internal {
    require(token.saleTransfer(_beneficiary, _tokenAmount));
  }

  /**
   * @dev Executed when a purchase has been validated and is ready to be executed. Not necessarily emits/sends tokens.
   * @param _beneficiary Address receiving the tokens
   * @param _tokenAmount Number of tokens to be purchased
   */
  function _processPurchase(address _beneficiary, uint256 _tokenAmount) internal {
    _deliverTokens(_beneficiary, _tokenAmount);
  }

  /**
   * @param _usdCents Value in usd cents to be converted into tokens
   * @return Number of tokens that can be purchased with the specified _usdCents
   */
  function _getTokenAmount(uint256 _usdCents) internal view returns (uint256) {
    uint256 tokens = _usdCents.div(tokenPrice).mul(1 ether);
    uint256 bonuses = 0;
    if (currentStage == Stages.PreSaleStart) bonuses = tokens.mul(presaleBonuses).div(100);
    if (currentStage == Stages.crowdsaleStageOneStart) bonuses = tokens.mul(stageOneBonuses).div(100);
    if (currentStage == Stages.crowdsaleStageTwoStart) bonuses = tokens.mul(stageTwoBonuses).div(100);
    if (currentStage == Stages.crowdsaleStageThreeStart) bonuses = tokens.mul(stageThreeBonuses).div(100);
    if (bonuses > 0) tokens = tokens.add(bonuses);
     
    return tokens;
  }

  function transferSoldTokens(address _beneficiary, uint256 _tokenAmount) public onlyOwner 
  {
      require(currentStage!=Stages.pause);
    uint256 tokenAmount = _tokenAmount.mul(1 ether);
    _validateTokensLimits(tokenAmount);
    require(token.saleTransfer(_beneficiary, tokenAmount));
  }
  
    function setEthPriceInCents(uint _ethPriceInCents) onlyOwner public returns(bool) {
        ethPrice = _ethPriceInCents;
        return true;
    }



  /**
   * @dev Determines how ETH is stored/forwarded on purchases.
   */
  function _forwardFunds() internal {
    wallet.transfer(msg.value);
  }
}
